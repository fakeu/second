class Accuracy {
  check(accuracy) {
    return accuracy < 5;
  }
}

class Road {
  check(count, textlength) {
    return `до финала осталось ${textlength - count} символов`;
  }
}

class Speed {
  check(count, timer) {
    return `при средней скорости ${count / (30 - timer)} символов в секунду`;
  }
}

class Statsman {
  constructor() {}

  applyFor(player) {
    const accuracyStat = new Accuracy().check(player.accuracy);
    const symPerSecond = new Speed().check(player.count, player.timer);

    const accuracyResult = accuracyStat
      ? `почти не совершает ошибок`
      : `много косячит`;

    const roadStat = new Road().check(player.count, player.textLength);
    return `${player.name} ${accuracyResult} ${symPerSecond} и ${roadStat}`;
  }
}

module.exports = new Statsman();
