class Winner {
  constructor() {
    this.msg = "you winn";
  }
}
class Looser {
  constructor() {
    this.msg = "you loose";
  }
}

class UserPostion {
  create(type) {
    let userPostion;
    if (type === "winner") {
      userPostion = new Winner();
    }
    if (type === "looser") {
      userPostion = new Looser();
    }
    userPostion.type = type;
    userPostion.info = function() {
      console.log(`Now ${this.msg}`);
    };
    return userPostion;
  }
}

module.exports = new UserPostion();
