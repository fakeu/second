const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");
const users = require("./users.json");
const statsman = require("./facade");
const bot = require("./bot");
const playerStatus = require("./factory");

require("./passport.config");

server.listen(3000);

app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/wait", function(req, res) {
  res.sendFile(path.join(__dirname, "wait.html"));
});

app.get("/game", function(req, res) {
  res.sendFile(path.join(__dirname, "game.html"));
});

app.get("/result", function(req, res) {
  res.sendFile(path.join(__dirname, "result.html"));
});

app.get("/text", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  res.send({
    text:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magni minima iure molestias. Repudiandae cupiditate nihil accusamus iure autem dolorem iste explicabo esse commodi quae repellat quasi itaque consequuntur, tempore similique?"
  });
});

app.get("/login", function(req, res) {
  res.sendFile(path.join(__dirname, "login.html"));
});

app.post("/login", function(req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, "someSecret", { expiresIn: "1000h" });
    res.status(200).json({ auth: true, token: "JWT " + token });
  } else {
    res.status(401).json({ auth: false });
  }
});

const resultTable = [];
let timers = {};
let i = 30;

io.on("connection", socket => {
  socket.on("gameend", payload => {
    const { token } = payload;
    let getClearToken = token.split(" ");
    const user = jwt.verify(getClearToken[1], "someSecret").login;
    resultTable.push({ ...payload, user });
  });
  socket.on("getresult", () => {
    socket.broadcast.emit("getresult", resultTable);
    socket.emit("getresult", resultTable);
  });

  socket.on("pressbtn", payload => {
    const { token } = payload;
    let getClearToken = token.split(" ");
    const user = jwt.verify(getClearToken[1], "someSecret").login;
    const data = { ...payload, name: user };
    socket.emit("pressbtn", statsman.applyFor(data));
  });

  socket.on("connectRoom", payload => {
    const { token, room } = payload;
    let getClearToken = token.split(" ");
    const user = jwt.verify(getClearToken[1], "someSecret").login;

    socket.join(room);

    socket.broadcast.emit("connectRoom", bot.sayHi(user));
    socket.emit("connectRoom", bot.sayHi(user));
  });

  socket.on("offer", payload => {
    const { token, room } = payload;
    let getClearToken = token.split(" ");
    const user = jwt.verify(getClearToken[1], "someSecret").login;

    clearInterval(timers[room]);
    timers[room] = setInterval(() => {
      if (i === 0) {
        i = 31;
      }
      if (i === 10) {
        socket.broadcast.emit("connectRoom", bot.sayTime("10"));
        socket.emit("connectRoom", bot.sayTime("10"));
      }
      io.sockets.in(room).emit("timerUpdate", i);
      i--;
    }, 1000);
  });
});
