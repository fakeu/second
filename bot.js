class Bot {
  constructor() {}
  sayHi(name) {
    return `${name} Connected to klava game`;
  }
  sayTime(time) {
    return `${time}sec left`;
  }
}
module.exports = new Bot();
