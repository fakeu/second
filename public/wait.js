window.onload = () => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) {
    location.replace("/login");
  } else {
    const socket = io.connect("http://localhost:3000");

    const time = document.getElementById("time");
    const botMsg = document.getElementById("bot-msg");

    socket.emit("connectRoom", { token: jwt, room: "wait" });
    socket.on("connectRoom", payload => {
      botMsg.innerText = payload;
    });

    socket.on("timerUpdate", data => {
      time.innerText = "Game start in: " + data;
      if (data === 1) {
        location.replace("/game");
      }
    });

    socket.emit("offer", { token: jwt, room: "wait" });
  }
};
