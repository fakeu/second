window.onload = () => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) {
    location.replace("/login");
  } else {
    const socket = io.connect("http://localhost:3000");
    const botMsg = document.getElementById("bot-msg");
    class Game {
      constructor() {}
      textField = document.querySelector("#text");
      body = document.querySelector("body");
      progressBar = document.querySelector("#progress");
      erCount = document.querySelector("#error-count");
      timeLeft = document.querySelector("#time");

      timer = 30;
      count = 0;
      accuracy = 0;
      progress = 0;
      textLength = 0;

      async start() {
        const text = await this.getText();
        const textArray = this.drawText(text);
        this.textLength = text.length;
        this.body.addEventListener("keypress", event => {
          this.pressEvent(event, textArray);
        });

        const tic = setInterval(() => {
          if (this.timer > 0) {
            this.timeLeft.innerText = "TIME LEFT: " + this.timer;
            if (this.timer === 10 || this.timer === 20) {
              socket.emit("pressbtn", {
                token: jwt,
                timer: this.timer,
                count: this.count,
                accuracy: this.accuracy,
                progress: this.progress,
                textLength: this.textLength
              });
            }
            socket.on("pressbtn", payload => {
              botMsg.innerText = payload;
            });
            this.timer--;
          } else {
            clearInterval(tic);
            this.endGame();
          }
        }, 1000);
      }
      endGame() {
        if (this.count === this.textLength || this.timer === 0) {
          socket.emit("gameend", {
            token: jwt,
            timer: this.timer,
            count: this.count,
            accuracy: this.accuracy,
            progress: this.progress,
            textLength: this.textLength
          });

          location.replace("/result");
        }
      }
      pressEvent(event, text) {
        if (event.key === text[this.count].innerText) {
          text[this.count].classList = "good";
          text[this.count + 1].classList = "current";
          this.count++;
          this.progress += 100 / this.textLength;
          this.progressBar.style.width = this.progress + "%";
          this.endGame();
        } else {
          text[this.count].classList = "error";
          this.accuracy++;
          this.erCount.innerText = this.accuracy;
        }
      }

      drawText(text) {
        const msgArray = text.split("").map(letter => {
          const span = document.createElement("span");
          span.innerText = letter;
          return span;
        });
        msgArray.map(element => {
          this.textField.appendChild(element);
        });
        return msgArray;
      }

      async getText() {
        let getClearToken = jwt.split(" ");
        return fetch("/text", {
          method: "GET",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${getClearToken[1]}`,
            "Content-Type": "application/json"
          }
        })
          .then(res => res.json())
          .then(data => data.text)
          .catch(err => {
            console.log(err);
          });
      }
    }

    const game = new Game();
    game.start();
  }
};
