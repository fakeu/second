window.onload = () => {
  const jwt = localStorage.getItem("jwt");
  if (!jwt) {
    location.replace("/login");
  } else {
    const socket = io.connect("http://localhost:3000");
    const botMsg = document.getElementById("bot-msg");

    const table = document.getElementById("table");
    const restart = document.getElementById("restart");
    restart.addEventListener("click", () => {
      location.replace("/");
    });

    socket.emit("getresult", {});
    socket.on("getmsgfrombot", payload => {
      console.log("WINNER IS " + payload.user);
    });
    socket.on("getresult", payload => {
      table.innerHTML = "";
      payload
        .sort((a, b) => {
          return b.progress - a.progress;
        })
        .map(element => {
          const li = document.createElement("li");
          const div = document.createElement("div");
          const span = document.createElement("span");
          div.classList = "progress-final";
          span.classList = "user-name";
          div.style.width = element.progress + "%";
          span.innerText = element.user;
          li.appendChild(div);
          li.appendChild(span);

          table.appendChild(li);
        });
    });
  }
};
